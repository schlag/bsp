\documentclass[11pt]{article}


% Für deutsche Trennungen, Datumsangaben etc.:
%\usepackage{german} % DON'T use if writing in English
% Um diakritische Zeichen (Umlaute etc.) direkt (=im Editor) einzugeben:
%\usepackage[utf8]{inputenc} % evtl. utf8 je nach System/Editor ERSETZEN, z.B. zu latin1
\usepackage{a4}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{graphicx} % include graphics with \includegraphics{xyz.pdf}
\usepackage[algoruled]{algorithm2e} % \begin{algorithm} ... \end{algorithm}
\usepackage{enumitem}
\usepackage[numbers]{natbib}
\usepackage{dsfont}
\usepackage{mdwlist} %compact lists

\usepackage{amsthm}
\newtheorem{thm}{Theorem}
\newtheorem{lem}[thm]{Lemma}
\newtheorem{cor}[thm]{Corollary}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{defi}{Definition}
\newtheorem{obs}{Observation}
\newtheorem{prob}{Problem}
\newtheorem{ex}{Example}

\DeclareMathOperator*{\argmax}{argmax}
\DeclareMathOperator*{\argmin}{argmin}

\pagestyle{plain}
%\parindent 0pt
%\parskip 11pt

\author{Sebastian Schlag}
\title{Seminar: \\
Algorithms in Realistic Parallel Models \\
A Bridging Model for Parallel Computation}
\date{\today}

\begin{document}
\maketitle
\nocite{*}

\paragraph{Remark} % (fold)
\label{par:Note}
This report was created for the seminar ''Algorithms in realistic parallel Models'' at the Karlsruhe Institute of Technology (KIT) and covers the paper ''A Bridging Model for parallel Computation'' \citep{Valiant}. It is intended as groundwork for the presentation that will be held in course of this seminar.
% paragraph bl (end)

\section{Introduction} % (fold)
\label{sec:introduction}
The cost of processors, memory and communication was decreasing for years at the time the paper was written. However, parallel machines still had not superseded sequential ones, although there was an increasing demand in computing power and numerous parallel machines already existed. The problem was that these machines where based on a multitude of different  architectures that needed \textit{architecture-tailored} parallel algorithms and software in order to be used efficiently. 

\citet{Valiant} identified the lack of a central unifying model of parallel computation as the possible cause of this situation.
While the von Neumann computer was used in sequential computation to act as a connecting bridge between the world of software and the world of hardware, a concrete model of parallel computation on which people could agree was still missing. 

The bulk-synchronous parallel (BSP) model was intended to close that gap by being something between a hardware and a programming model \cite{Valiant}. It aimed to insulate hardware and software development from one another to pave the way for future general purpose parallel computers and transportable software.

% section introduction (end)
\section{The BSP Model} % (fold)
\label{sec:bsp_model}
The BSP model consists of three components: an abstract computer architecture, a class of (bulk-synchronous) algorithms and a cost model that supports the design and analysis of such BSP algorithms and allows for performance predictions on concrete architectures. Thus, it can be seen as a \textit{unified framework for the design, analysis, and programming of general purpose parallel computing systems} \citep{Gerbessiotis1996}.

As we will see in this section, the term \textit{bulk-synchronous} reflects the model's position between the two extremes of a completely synchronous system (where global synchronization happens on every instruction) and a completely asynchronous system (that relies only on pairwise synchronization for communication purposes) \citep{Gerbessiotis1996}.

In general, a \textbf{BSP computer} can be understood as a distributed-memory machine. It consists of a set of $p$ processors (each of which has its own private memory), a communication network\slash router (see figure \ref{fig:bspmachine}) and a mechanism for barrier synchronization for all (or a subset of all) processors. Every processor is able to perform read and write operations on every memory location. While local computation steps and local memory accesses can be performed in one time unit, remote memory access is slower due to the delay of the communication network. However, the model assumes that the access time is the same for all remote memory accesses. Since despite point-to-point communication and global barrier synchronization there are no further assumptions on the communication network (e.g. a certain network topology or existing broadcasting or combining mechanisms), it can be regarded as a black box.

\begin{figure}[h!tbp]
 	\centering
 	\includegraphics[width=0.4\textwidth]{pics/bspMachine.pdf}
 	\caption[]{The BSP machine architecture. The figure is based on \citep{heide}.}
 	\label{fig:bspmachine}
 \end{figure}

 \textbf{BSP algorithms} consist of a sequential sequence of \textit{supersteps} (see figure \ref{fig:bspalgo}) that is executed by a set of $v$ virtual processors. These virtual processors (i.e. processes\slash threads) are distributed uniformly at random among the $p$ real processors. A superstep can be seen as the \textit{basic parallel instruction} of the BSP computer \cite[Ch.~4]{dbs}. It is divided into three consecutive phases:
 \begin{itemize*}
    \item \textit{Computation}: Each processor performs a series of computation steps using local data that is available at the beginning of the superstep.
    \item \textit{Communication}: During the computation phase, each processor can perform message transmissions (i.e. remote read-\slash write requests). The messages are queued until the end of the computation phase and then sent en masse in the communication phase.
    \item \textit{Barrier synchronization}: After all message transmissions are complete a barrier ensures that all processors have reached the same point in the BSP algorithm. At this point all received messages will be made available in the local memories and all processors collectively proceed to the next superstep.
  \end{itemize*} 

   \begin{figure}[h!tbp]
 	\centering
 	\includegraphics[width=0.7\textwidth]{pics/bspSupersteps.pdf}
 	\caption[]{The execution of a BSP algorithm. BSP programs have a horizontal and vertical structure \citep{skill}. The vertical structure is given by the sequential sequence of supersteps. The horizontal structure is due to the concurrency of the virtual processors.  The figure is based on \citep{heide}.}
 	\label{fig:bspalgo}
 \end{figure}

 Communication is thus both \textit{blocking} and \textit{non-blocking}. It is \textit{blocking} in that all processors must wait at the end of a superstep until the global barrier synchronization phase is complete in order to access received messages. It is also \textit{non-blocking}, since a processor can continue its computation after sending a message. 

The concept of $h$-relations is used to analyze this bulk message transfer. An $h$-relation is a communication pattern where every processor sends and receives at most $h$ messages and at least one processor sends or receives $h$ messages. A message thereby contains one data word. This communication model is motivated by the assumption that the bottleneck of the communication is not the network itself but it's entry and exit points.

The cost of realizing such an $h$-relation is $$T_{\textrm{h-relation}}(h)= hg + L.$$
$L$ is the minimum elapsed time between successive barrier synchronizations. $g$ can be seen as the ratio of the total number of local operations performed by all processors, to the total number of messaged delivered per second by the router: $$\frac{\textrm{total computation throughput}}{\textrm{total communication throughput}}=\frac{phg}{ph}=g,$$ since in the time period of an $h$-relation the machine can perform $phg$ local operations in total while the router is able to transport at most $ph$ messages.
$g$ can also be regarded as the time needed (measured in computation steps) to send\slash receive one message in the situation of continuous message traffic \citep{bis}, since: $$\lim_{h \to \infty} \frac{hg+ L}{h}=g.$$

Both parameters $g$ and $L$ are machine-dependent. Therefore their values are determined by benchmarking the target architecture with a range of \textit{full} $h$-relations - i.e. $h$-relations, where \textit{every} processor sends \& receives exactly $h$ messages . Thus $g$ and $L$ can be seen as an upper bound on the cost of an arbitrary $h$-relation, since the benchmark uses worst case communication patterns (see figure \ref{fig:hrelation}). 

\begin{figure}[h!tbp]
 	\centering
 	\includegraphics[width=0.6\textwidth]{pics/hrelation.pdf}
 	\caption[]{Two different $h$-relations that have the same $h$-value. a) A 2-relation with $h_\textrm{send}=2$ and $h_\textrm{receive}=1$, b) a \textit{full} 2-relation with $h_\textrm{send}=2$ and $h_\textrm{receive}=2$.}
 	\label{fig:hrelation}
 \end{figure}

Note that $T_{\textrm{h-relation}}$ does not explicitly account for any startup overheads. This is due to the fact that communication happens en masse at the communication phase of a superstep. This makes it possible to combine messages to the same destination. Therefore startup overhead has to be accounted for only once per processor per superstep and can be folded into the value of $g$ \citep{skill}. 

Also note that possible network congestion and hot spots are modeled implicitly through the concept of $h$-relations: A message pattern where every processor only sends and receives one message counts as an $1$-relation, whereas a broadcast from one processor to all other processors counts as a $(p-1)$-relation. Unbalanced communication patterns that are more likely to cause congestion are thus charged more than balanced onces.

After commuinication, the barrier synchronization concludes a superstep. \citet{Valiant} proposes two different ways of how this global synchronization could be achieved:
\begin{itemize*}
	\item A global ''synchronizer'' checks every $L$ time units if all processes that should be synchronized in the current superstep have finished. In case they have, the machine proceeds to the next superstep. Otherwise the next period of $L$ time units is allocated to the current superstep.
	\item Alternatively, a superstep can complete any time after the first $L$ time units. Therefore the system continuously checks if the current superstep is completed and proceeds to the next superstep as soon as possible. Since this is variant is used more frequently in literature, we will use this approach for the analysis of the algorithms presented in section \ref{sec:algorithms}.
\end{itemize*}

Originally \citet{Valiant} focused on the \textit{global synchronizer} method and defined the \textbf{cost} of a superstep as: $$T_{\textrm{superstep}} = \max(w,hg,L).$$ If we denote the local work performed by processor $i$ with $w_i$, then $w=\max_i(w_i)$. In case a superstep can complete any time, a more practical cost function can be used:
$$T_{\textrm{superstep}} = w + hg + L.$$ 
The following example \citep{book} shows the benefits of using the second approach: Consider two supersteps without computation ($w=0$), where communication takes place in form of a $2$-relation in the first and in form of a $3$-relation in the second superstep. Using the original cost function the cost of both supersteps together is $\max(2g,L)+\max(3g,L)$. This formula may have any of the following outcomes: $5g$, $2g+L$ or $2L$. The second cost function simply charges $5g+2L$. In order to avoid these ambiguities of the original cost function, we will use the second one in section \ref{sec:algorithms}. According to \citet{Valiant} the actual difference between these to functions in the results of the run-time analysis is within small constant factors and thus negligible.

The runtime of an entire BSP algorithm is defined in both cost models as the sum over the run-times of all supersteps: 
$$T_{\textrm{algorithm}} = \sum T_{\textrm{superstep}}.$$

Based on this model, a BSP algorithm has to achieve the following design goals in order to be efficient:
\begin{itemize*}
  \item Minimize the number of supersteps and thus the number of barrier synchronizations.
  \item Achieve good load balancing within a superstep: $\max_i(w_i)\xrightarrow{\\!} \min$
  \item Balance the communication between processors: $h=\max_i(h_i)\xrightarrow{\\!} \min$
\end{itemize*}

Since BSP was aimed to be a bridging model that connects the SW and HW world, it should not only be useful for theoretical algorithm design but also suitable to be used as a programming model. \citet{Valiant} therefore envisioned two different modes of programming that are mostly concerned about how data is distributed. 

In \textit{automatic mode} the runtime-system takes care of memory distribution and management, communication assignment and synchronization in a transparent manner and, thus, creates a shared-memory view for the user. It uses hashing and randomization to distribute the $v$ virtual processors, memory accesses and communication among the $p$ real processors to guarantee good performance with high probability. This mode allows for PRAM-style programming and is able to simulate the most general CRCW-PRAM model \citep{Valiant}. In fact in the ideal case that $g=L=1$ BSP equals PRAM. The only constraint for using this mode is that the BSP program has to exhibit sufficient \textit{parallel slackness}. This means that programs have to be written for $v = \Omega( p \log p)$ virtual processors so that the compiler is able to schedule computation and communication efficiently such that the latency of communication is hidden by computation.%the machine is able to hide the latency of communication by giving each real processor many different tasks so that they can work on the tasks that are ready, while others are blocked (e.g. waiting on communication).

In \textit{direct mode} the programmer retains control about memory management, communication assignment and synchronization and has to ensure that the data that is used for local computation is in local memory at the beginning of each superstep. Algorithms are thus implemented directly in the model, not using any abstraction layer like \textit{automatic mode}. This approach is especially useful in cases where $g$ increases, since it allows for optimizing communication appropriately. Therefore this mode will be used in the following chapter.

% section bsp_model (end)
\section{Algorithms} % (fold)
\label{sec:algorithms}
Analyzing an algorithm in the BSP model is done by breaking it down into supersteps and determining the work $w_i$ done by each processor and the communication volume $h$. It is then possible to show which assumptions for parameters $g$ and $L$ have to be true to achieve an optimal BSP algorithm. A BSP algorithm is considered optimal, if the time-processor product exceeds the number of computational operations performed by the sequential algorithm by only a fixed multiplicative constant \citep{Valiant}: $$p~T_{\textrm{BSP}} = \mathcal{O}(T_{\textrm{seq}}),$$ independent of $L$, $g$, $p$ and the size $n$ of the problem instance.  %A BSP algorithm is optimal if its runtime is in $\mathcal{O}\left(\frac{T_{\textrm{seq}}(n)}{p}\right)$, where $T_{\textrm{seq}}(n)$ is an upper bound on the sequential runtime. 

This section covers a selection of the algorithms described in the original article: matrix multiplication, sorting and broadcasting. We assume that $v=p$.

\subsection{Matrix Multiplication} % (fold)
\label{sub:matrix_multiplication}
Consider the problem of multiplying two matrices $A \in \mathds{R}^{n \times n}$ and $B \in \mathds{R}^{n \times n}$ with $ A = ((a_{ij}))$ and $B = ((b_{ij}))$. Then the entries of $C \in \mathds{R}^{n \times n}$ where $C = AB = ((c_{ij}))$ are defined by $$c_{ij} = \displaystyle\sum_{k=1}^n a_{ik} b_{kj}.$$ The sequential standard algorithm computes $C$ in $\mathcal{O}(n^3)$ time.  Our goal is create a parallel  BSP-version of this algorithm that achieves optimal runtime $\mathcal{O}\left(\frac{n^3}{p}\right)$ using $p \leq n^2$ processors. 

The key idea of the parallel algorithm is to assign each processor a sub-problem of computing an $\frac{n}{\sqrt{p}} \times \frac{n}{\sqrt{p}}$ sub-matrix of $C$ (see figure \ref{fig:matrix}). In order to solve this sub-problem, each processor needs a total of $\frac{n}{\sqrt{p}}$ rows of matrix $A$ and $\frac{n}{\sqrt{p}}$ columns of matrix $B$ as input. 

We cannot expect to have the elements of matrices $A$ and $B$  spread over the processors exactly as we need. Therefore we assume that they are \textit{uniformly distributed} among the processors such that each processor has $\frac{n^2}{p}$ elements of $A$ as well as $\frac{n^2}{p}$ elements of $B$ in its local memory. In the worst distribution that could possibly appear, each processor initially has elements in it's local memory that aren't needed for solving it's sub-problem.

%fig here?
 
 The BSP algorithm consists of two supersteps:
\begin{itemize}
	\item  In the first superstep, each processor has to send each one of its local values to $\sqrt{p}$ other processors. Therefore each processor locally replicates each element $\sqrt{p}$ times and sends the copies to the $\sqrt{p}$ processors that need them. This leads to a total number of $\frac{2n^2}{p}~\sqrt{p} = \frac{2n^2}{\sqrt{p}}$ message transmissions per processor. The runtime of the first superstep therefore is:

$$T_{\textrm{superstep 1}}=\underbrace{\sqrt{p}~\frac{2n^2}{p}}_{\textrm{local replication}}+
  \underbrace{\frac{2n^2}{\sqrt{p}}~g}_{\textrm{message exchange}}+ \underbrace{L}_{\textrm{barrier sync}}.$$
  \item In the second superstep, each processor then calculates the assigned $c_{ij}$  and thereby performs $n~\frac{n}{\sqrt{p}}~\frac{n}{\sqrt{p}} = \frac{n^3}{p}$ multiplications and the same number of additions. The runtime of the second superstep is:

$$T_{\textrm{superstep 2}}=\underbrace{\frac{2n^3}{p}}_{\textrm{local multiplications \& additions}}+
   \underbrace{L}_{\textrm{barrier sync}}.$$
\end{itemize}
 
  Thus the total cost of this BSP algorithm is $$T_{\textrm{matrix-mult}}=T_{\textrm{superstep 1}}+T_{\textrm{superstep 2}}=\mathcal{O}\left(\frac{n^3}{p}+
  \frac{n^2}{\sqrt{p}}~g+ L\right)$$ which is in $\mathcal{O}\left(\tfrac{n^3}{p}\right)$ for $g \in \mathcal{O}\left(\tfrac{n}{\sqrt{p}}\right)$ and $L \in \mathcal{O}\left(\tfrac{n^3}{p}\right)$.

  \begin{figure}[h!tbp]
 	\centering
 	\includegraphics[width=0.8\textwidth]{pics/matrix.pdf}
 	\caption[]{Parallel matrix multiplication with $n=4$ and $p=4$. Matrix elements that are stored at processor $0$ as well as the sub-matrix $\bigl(\begin{smallmatrix} c_{11}&c_{12}\\ c_{21}&c_{22} \end{smallmatrix} \bigr)$ that it has to compute are highlighted in gray. In this case we have a worst case distribution for processor $0$, since it doesn't need any of its local elements for computation.}
 	\label{fig:matrix}
 \end{figure}

% subsection matrix_multiplication (end)

\subsection{Sorting} % (fold)
\label{sub:sorting}
Columnsort \citep{Leighton} is a sorting algorithm that can be directly mapped to a bulk synchronous algorithm. For sorting $n$ items on $p=\mathcal{O}(n^\frac{1}{3})$ processors, we distribute the elements in form of a $\mathcal{O}\left(n^\frac{2}{3} \times n^\frac{1}{3}\right)$ matrix and let each processor be in charge of one column of $\mathcal{O}\left(n^\frac{2}{3}\right)$ items. 

Columnsort then performs eight consecutive steps - each of which can be executed in one superstep:
\begin{itemize}
	\item In the odd-numbered steps 1,3,5 and 7, each processor sorts its local set of $\mathcal{O}\left(\frac{n}{p}\right)	=\mathcal{O}\left(n^\frac{2}{3}\right)$ items in time 
	$$T_{\textrm{sort-step}}=\mathcal{O}\left( \frac{n}{p} \log \frac{n}{p} \right) + L$$ using a standard sequential sorting algorithm.
	\item In the even-numbered steps 2,4,6 and 8, the data is permuted among the processors in specific patterns:
		\begin{itemize}
		 	\item In step 2 the data is permuted such that the matrix elements are ''transposed'' - elements are picked up column-wise and sent to other processors such that they are then deposited row-wise (see step 2 in figure \ref{fig:sort}). Step 4 performs the inverse operation of step 2.
		 	\item The permutation in step 6 is an $\lfloor\frac{n^\frac{2}{3}}{2}\rfloor$-shift of the entries and step 8 performs the reverse operation.
		 \end{itemize} 
\end{itemize}
     Each of these permutation-supersteps runs in
     $$T_{\textrm{permute-step}}=\mathcal{O}\left( \frac{n}{p}~g \right) + L \textrm{ time.}$$ 

      In order to achieve optimal runtime $\mathcal{O}\left( \frac{n}{p} \log \frac{n}{p} \right)$ on the BSP model, we have to ensure that the communication time $T_{\textrm{permute-step}}$ does not exceed the computation time $T_{\textrm{sort-step}}$. If $g=\mathcal{O}\left(\log \frac{n}{p} \right)$ and $L = \mathcal{O}\left( \frac{n}{p} \log \frac{n}{p} \right)$ this constraint is fulfilled.
 This algorithm is an example of bulk synchronous algorithms that separate computation and communication into two supersteps.

 \begin{figure}[h!tbp]
 	\centering
 	\includegraphics[width=0.7\textwidth]{pics/sort.pdf}
 	\caption[]{Step by step execution of columnsort assuming we got 3 processors - one for each column. The $-\infty$ and $\infty$ items denote arbitrarily small resp. large dummy items. The figure is based on \citep{Leighton}.}
 	\label{fig:sort}
 \end{figure}


% subsection sorting (end)

\subsection{Broadcast} % (fold)
\label{sub:broadcast}
Broadcasting is an example that shows the necessity of balancing the communication between processors. Suppose that processor $0$ needs to send one message to each of $n>p$ memory locations spread uniformly among all other processors ($\mathcal{O}\left(\frac{n}{p}\right)$ per processor). This might be the case when $v > p$ \textit{virtual processors} are distributed among the $p$ physical processors. In that case one virtual processor wants to broadcast a value to all other virtual processors.

In this case, it would be inefficient to perform a naive broadcast that replicates the message $n$ times at the source and sends $\mathcal{O}\left(\frac{n}{p}\right)$ copies to each of the $p-1$  other processors in one superstep:
%A first naive broadcasting algorithm simply replicates the message n times sends the message out $p-1$ times in one superstep (see figure \ref{fig:naiveBC}). In the next superstep each processor then locally replicates the message $\frac{n}{p}$ times. Analyzing the cost of this algorithm however shows its inefficiency:
$$T_\textrm{naiveBC} = \underbrace{n}_{\textrm{local replication}}~+~\underbrace{\mathcal{O}\left(\frac{n}{p}~p\right)g}_{\textrm{communication}}~+~L = n~+~\mathcal{O}(n)~g~+L .$$



While it takes only one superstep to distribute the message to the $n$ memory locations, the algorithm has two major drawbacks: First, it exhibits bad load balancing, since only processor $0$ performs local operations while all other processors are idle. Second, the distribution follows a badly balanced communication pattern: A $\mathcal{O}(n)$-relation, where only processor $0$ sends $\mathcal{O}(n)$ messages and all other processors only receive $\mathcal{O}\left(\frac{n}{p}\right)$ messages each. 
  \begin{figure}[h!tbp]
  \centering
  \includegraphics[width=0.6\textwidth]{pics/naiveBC.pdf}
  \caption[]{A naive broadcast leads to an imbalanced $\mathcal{O}(n)$-relation communication pattern.}
  \label{fig:naiveBC}
 \end{figure}

In order to achieve a more balanced communication pattern, a $d$-ary tree topology (as shown in figure \ref{fig:treeBC} for $d=2$) can be used to achieve a communication-synchronization trade-off. In each superstep, all processors on the current level $l$ of the tree transmit $d$ copies to the processors on level $l+1$. Thus it takes $\log_d p$ supersteps until all processors have received the message. In a final superstep each processor then creates $\mathcal{O}\left(\frac{n}{p}\right)$ local copies. The cost of this algorithm therefore is:
$$T_{\textrm{d-ary tree}}= \underbrace{(d~g + L)~\log_d p}_{\textrm{communication}}~+~\underbrace{\mathcal{O}\left(\frac{n}{p}\right) + L}_{\textrm{local replication}}.$$

As can be seen, local work is now equally distributed among all processors and the communication cost has been reduced to a $d$-relation, but at the expense of factor $\log_d p$ more barrier synchronizations.
  \begin{figure}[h!tbp]
  \centering
  \includegraphics[width=0.6\textwidth]{pics/treeBC.pdf}
  \caption[]{$d=2$-ary tree broadcast}
  \label{fig:treeBC}
 \end{figure}

However if $d=\mathcal{O}\left(\frac{n}{gp\log p} \log\left(\frac{n}{gp\log p}\right) \right)$, $L=\mathcal{O}(gd)$ and thus $n = \Omega(gp\log p)$  we achieve optimality:
\begin{align*}
 T_{\textrm{d-ary tree}} &=\mathcal{O}\left(dg \log_d p + \frac{n}{p} + gd \right)\\
  &= \mathcal{O}\left( \frac{n}{gp\log p} \log\left(\frac{n}{gp\log p}\right)~g~\frac{\log p}{\log \frac{n}{gp\log p}} + \frac{n}{p} + g~ \frac{n}{gp\log p} \log\left(\frac{n}{gp\log p}\right) \right) \\
 &= \mathcal{O}\left(\frac{n}{p}\right).
\end{align*}

% subsection broadcast (end)

% section algorithms (end)

\section{Conclusion} % (fold)
\label{sec:conclusion}
After having introduced the BSP model in section \ref{sec:bsp_model} and having analyzed a couple of algorithms in section \ref{sec:algorithms} the question remains if BSP is able to fulfill the requirements of a central unifying model of parallel computation?
According to \citet{Valiant}, there are three kinds of arguments in favor of BSP as a bridging model for parallel computation:

From a \textit{software} perspective, the major advantage of BSP is its \textit{automatic} programming mode that provides a virtual shared memory view to the programmer. This allows for easier, PRAM-style programmability and can be realized with only a constant factor loss in efficiency if $g$ is reasonably small. 

From an \textit{algorithm} perspective, it is shown that it is possible to design several algorithms directly on the BSP model. These algorithms could be implemented on a real BSP machine using \textit{direct mode}.

Finally from a \textit{hardware} perspective, the model can be implemented in many different communication technologies and on various network topologies, because the only constraints that have to be fulfilled are high throughput and a way to perform barrier synchronization.

On top of that, BSP captures the features of parallel machines in only three parameters: $p$, $g$ and $L$.
On the one hand, this allows hardware designers to focus on decreasing $g$, $L$ and increasing $p$. On the other hand, software designers can concentrate on minimizing the amount of computation and communication and the frequency of barrier synchronization and do not have to care about the specific details of the actual machines their programs will run on.

Taking all this into account, one can certainly argue that BSP has been a promising candidate as a bridging model for general-purpose parallel computation at the time the paper was published.
% section conclusion (end)


 % \begin{figure}[h!tbp]
 % 	\centering
 % 	\includegraphics[width=0.6\textwidth]{pics/}
 % 	\caption[]{}
 % 	\label{fig:}
 % \end{figure}


\bibliographystyle{plainnat}
%\label{lib}
\bibliography{lib}

\end{document}
